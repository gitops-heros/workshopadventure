#!/bin/bash

## 
set -e
DIR=$(dirname "$0")

pushd $DIR

echo "🏗️ Installing Dhall Config Management Plugin for ArgoCD"
set -x

kubectl apply -n argocd -f ./0-install-2.6.3-patched.yml

{ set +x; } 2> /dev/null # silently disable xtrace

popd

