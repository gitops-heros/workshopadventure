#!/bin/bash
echo "Login to argocd"

if [ -z "$IP" ]
then
    ## GET IP
    if [ "x$1" == "x" ]
    then
        context=$(kubectl config current-context)
    else    
        context=$1
    fi
    IP=$(kubectl get svc traefik-ingress-controller -n kube-system -o jsonpath='{ .status.loadBalancer.ingress[0].ip }')
fi

set -x
argocd login \
    --insecure \
    --username admin \
    --grpc-web \
    argocd.$IP.sslip.io


