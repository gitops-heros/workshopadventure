#/bin/sh
# from https://www.hashicorp.com/blog/retrieve-hashicorp-vault-secrets-with-kubernetes-csi

## install vault
echo "🏗️ Installing HashiCorp Vault (in dev mode in the cluster, please don't do this in production)"
kubectl create ns vault
kubectl apply -f manifests/vault.yaml -n vault --wait=true
## wait for vault
echo "⏳ Waiting for Vault to be ready"
kubectl wait --for=condition=Ready pod/vault-0 -n vault --timeout=30s
while [ "1" != $(kubectl get sts vault -n vault -o=custom-columns=READY:.status.readyReplicas --no-headers) ]
do
    printf "."
    sleep 1
done
echo " ✅"
echo "Vault is Ready"


## install csi driver
echo "🏗️ Installing HashiCorp Vault csi provider"
kubectl apply -f manifests/vault-csi-provider.yaml -n vault --wait=true

echo "🏗️ Installing CSI driver"
kubectl apply -f manifests/secret-store-csi-driver.yaml -n vault --wait=true
kubectl apply -f manifests/secrets-store.csi.x-k8s.io_secretproviderclasses.yaml -f manifests/secrets-store.csi.x-k8s.io_secretproviderclasspodstatuses.yaml

kubectl rollout status ds vault-csi-provider -n vault --timeout=60s
kubectl rollout status ds csi-secrets-store-csi-driver -n vault --timeout=60s
echo "CSI is deploy"

## configure vault (with script)
echo "Configure vault (enable kube auth...)"
kubectl cp ./configure_vault.sh vault/vault-0:/tmp/configure_vault.sh
kubectl exec -it vault-0 -n vault -- /bin/sh /tmp/configure_vault.sh