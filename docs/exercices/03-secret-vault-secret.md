# Fonctionnement des secrets Vault

Il existe plusieurs façons d'injecter des secrets [Vault](https://www.vaultproject.io/) dans son application de façon sécurisée. Une de ces façons est l'utilisation de [BanzaiCloud bank vault](https://github.com/banzaicloud/bank-vaults) qui va permettre d'installer et de configurer dans votre cluster un Vault et de fournir un [webhook](https://github.com/banzaicloud/bank-vaults/tree/main/cmd/vault-secrets-webhook) permettant de **transformer** vos descripteurs s'il rencontre un certain pattern sur certaines resources (PodSpec, Secret, ...).

Pour créer un secret, il suffit donc d'utiliser la même command-line que pour créer un secret dans un Vault classique à la différence que votre Vault est installé dans votre cluster.

Pour récupérer toutes les variables nécessaires à la connexion à vault:

```sh
export VAULT_TOKEN=$(kubectl -n vault-operator get secrets vault-unseal-keys -o jsonpath={.data.vault-root} | base64 --decode)
```

Pour créer un secret:

```sh
kubectl -n vault-operator exec vault-0 -c vault -- /bin/sh -c "VAULT_TOKEN=$VAULT_TOKEN VAULT_CACERT=/vault/tls/ca.crt vault kv put secret/my-secret content='my very secure secret'"
```

Pour ensuite faire l'association avec un de vos pods il suffira de par exemple créer un `Secret` Kubernetes en demandant au webhook de résoudre la valeur à votre place :

```sh
SECRET=$(echo -n '${vault:secret/data/my-secret#content}' | base64 -w0)
cat > secret.yml <<EOF
---
apiVersion: v1
kind: Secret
metadata:
  name: my-secret
  annotations:
    vault.security.banzaicloud.io/vault-addr: "https://vault.vault-operator:8200"
    vault.security.banzaicloud.io/vault-tls-secret: "vault-tls"
    # Permet de notifier le webhook d'aller inspecter le contenu du secret.
    vault.security.banzaicloud.io/inline-mutation: 'true'
data:
  secret: ${SECRET}
EOF
kubectl apply -f secret.yml
```

Ici les variables `vault.security.banzaicloud.io/vault-addr`, `vault.security.banzaicloud.io/vault-tls-secret` et `vault.security.banzaicloud.io/inline-mutation` vont permettre respectivement de préciser quel vault doit être consulté, l'AC permettant de vérifier que c'est bien le bon vault (validation du certificat serveur) et de dire au webhook de venir modifier la valeur base64 incluse dans le descripteur.

Après avoir appliqué ce secret, vous pouvez directement le retrouver dans les secrets kube avec la valeur résolue.

```sh
$ kubectl get secret my-secret -o yaml
...
apiVersion: v1
kind: Secret
metadata:
  name: my-secret
  annotations:
    vault.security.banzaicloud.io/vault-addr: "https://vault.vault-operator:8200"
    vault.security.banzaicloud.io/vault-tls-secret: "vault-tls"
    # Permet de notifier le webhook d'aller inspecter le contenu du secret.
    vault.security.banzaicloud.io/inline-mutation: 'true'
data:
  secret: bXkgdmVyeSBzZWN1cmUgc2VjcmV0
...
$ echo -n bXkgdmVyeSBzZWN1cmUgc2VjcmV0 | base64 -d
my very secure secret
```

Il vous suffit donc après de venir injecter votre secret dans votre pod pour l'utiliser.
