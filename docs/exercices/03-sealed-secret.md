# Principe

[Bitnami sealed secret](https://github.com/bitnami-labs/sealed-secrets) est une solution de chiffrage des secrets utilisant du chiffrement asymétrique.  
La clef privée est sur le serveur, la clef publique est disponible pour tous et sert à chiffrer le secret. Cela permet d'avoir des secrets qui sont versionnable car seul le serveur est en mesure de les déchiffrer.

Il y a une ressource spécifique de type `SealedSecret` qui contient le(s) secret(s) chiffré(s) avec la clef publique.  
Le `SealSecret` est transformé et `Secret` par un operateur qui connait la clef privée.

## Installation

**Depuis votre livre de sorts (répertoire `/workspace/spell-book/`).**

Installer l'opérateur depuis le dossier `secrets/sealedsecrets` depuis le dossier Gitpod contenant le livre des sorts (le premier)

```shell
./01-install-sealedsecret.sh
```

## Usage

### Créer un sealed secret

**Depuis votre livre de sorts (répertoire `/workspace/spell-book/`).**

Si l'on veut pouvoir générer les sealed secret sans connexion au cluster (utile par exemple pour de la prod)

Le certificat publique a été récupérer par le script d'installation. Si vous voulez le récupérer vous pouvez utiliser le scripts

```shell
./02-get-public-cert.sh
```

Cette clef sera stockée dans le fichier `public-cert.pem`

Il y a différents niveaux de sécurisation proposés par les sealed secret, pour faire au plus simple, vous allez utiliser la version dite "cluster-wide".
Pour créer les sealed secret il y un script `03-gen-secret.sh`.  
Si l'on veut créer les sealead secret contenant : le `secret` de nom "james-secrets" dans le `namespace` "kustomize-secret" contenant "secret='👋 Hello from Bitnami SealedSecret :)'" (les secrets contiennent des entrées "clef-valeur")

```shell
./03-gen-secret.sh james-secrets kustomize-secret secret "👋 Hello from Bitnami SealedSecret :)"
```

Ce script génère les différents type de sealed secret que vous pourrez étudier ulterieurement (utilisez le cluster-wide, c'est le plus facile à mettre en oeuvre).

Récupérez le sealed secret dit "cluster-wide" et reportez cette valeur (et poussez dans git) dans le descripteur de l'environnement "secret". En fonction du modèle de déployement utilisé les modifications porteront sur des objets différents.

Indice: en fonction de la solution utilisée cherchez:

- helm : `sealedSecrets`
- kustomize : `encryptedData`
- dhall : `sealedSecret`

**Le secret a déployer doit contenir '👋 Hello from Bitnami SealedSecret :)'**
