# Contexte

Argo CD est un opérateur qui va déployer des "applications" depuis leurs descripteurs versionnés dans un repo git.

Argo CD va utiliser comme kube un principe d'état désiré.

- on décrit ce que l'on veut et pas le comment
- il y a une boucle de contrôle qui fait tendre "l'application" vers son état désiré
- si par ailleurs des composants de l'application sont modifiés "à la main" il y aura soit un retour à l'état désiré, soit de la notification de la divergence

Plus d'informations [🔗 ArgoCD](https://argo-cd.readthedocs.io/en/stable/)

# Installation de Argo CD dans le cluster K8S

**Depuis votre livre de sorts (répertoire `/workspace/spell-book/`).**  
Il faut que vous puissiez passer des commandes kubectl.

👀 Chapitre (aka sous dossier) `argo`

Si ce n'est pas fait faite un `export` de l'IP du load balancer

```
source ../civo-k3s/get-cluster-ip.sh
```

Installer argo-cd

```shell
./01-install_argo.sh
```

Ce script va :

- créer un namespace pour argocd.
- installer argocd.
- changer le mot de passe (attention, c'est un mot de passe de demo).
- créer une ingress route pour exposer le dashboard de ArgoCD.

Les informations de connexion au dashboard seront affichés dans le terminal par le script d'installation, soit

> https://argocd.$IP_DU_LOAD_BALANCER.sslip.io (user: admin | pwd: argodemo)

## Validation de l'étape en deployant le hello world

1. Nous utiliserons ici le répertoire `deploy-sith-from-gitlab` qui contient les descripteurs des applications qui seront déployées.

Il y a aussi inclus, via la magie élémentaire de GitPod, le projet de livre de sort (https://gitlab.com/gitops-heros/workshopadventure)

2. Configurez **les descripteurs d'applications qui sont dans /workspace/deploy-sith-from-gitlab** pour votre cluster.

- ouvrez dans un shell le dossier ` /workspace/deploy-sith-from-gitlab` (un nouveau ou un shell existant)
- mettez à jour l'ip à laquelle vos applications seront visibles, vous avez dû récupérer l'IP du loadBalancer avec le script `/workspace/spell-book/civo-k3s/get-cluster-ip.sh` notez cette IP (pour l'exemple ce sera 212.2.243.105)

**Depuis votre sac contenant les applications (répertoire `/workspace/deploy-sith-from-gitlab/`).**

```shell
# 👀 212.2.243.105 est une IP d'exemple, mettez l'IP de votre LOAD BALANCER
./change-ip.sh 212.2.243.105
```

Vous devez avoir en sortie console quelque chose comme

```
🪄 Update ./01-static-yaml/ingress.yml
127.0.0.1.sslip.io -> 212.2.243.105.sslip.io
🪄 Update ./02-helm/values-sealed.yaml
127.0.0.1.sslip.io -> 212.2.243.105.sslip.io
🪄 Update ./02-helm/values-staging.yaml
127.0.0.1.sslip.io -> 212.2.243.105.sslip.io
🪄 Update ./02-helm/values.yaml
127.0.0.1.sslip.io -> 212.2.243.105.sslip.io
🪄 Update ./02-kustomize/base/ingress.yml
127.0.0.1.sslip.io -> 212.2.243.105.sslip.io
🪄 Update ./02-kustomize/overlays/production/ingress.yml
127.0.0.1.sslip.io -> 212.2.243.105.sslip.io
🪄 Update ./02-kustomize/overlays/secret/ingress.yml
127.0.0.1.sslip.io -> 212.2.243.105.sslip.io
🪄 Update ./02-kustomize/overlays/staging/ingress.yml
127.0.0.1.sslip.io -> 212.2.243.105.sslip.io
```

- commitez et poussez le tout dans le repository gitlab

```shell
git commit -a -m "🖊️ mise à jour l'ip du load balancer" && git push
```

3. Configurez les Apps argocd pour qu'elles utilisent ce repository gitlab comme source de vérité

**Depuis votre livre de sorts (répertoire `/workspace/spell-book/`).**

- allez dans le dossier des applications ArgoCD `argo-apps` pour mettre à jour l'URL du repository git.
  Si par exemple l'url de votre repository est https://gitlab.com/dev/deploy-sith-from-gitlab.git

```
./change-repo.sh https://gitlab.com/dev/deploy-sith-from-gitlab.git
```

4. Déployez l'application "Hello World" et validez l'étape

> ⚠️️ Cette application "hello world" est une application d'exemple, elle sert juste à valider cette étape, une fois cette étape finie oubliez cette application !

Pour déployer l'application, il nous suffit d'utiliser le manifest `01-hello-world.yml`

```shell
kubectl apply -f 01-hello-world.yml
```

Ouvrez ensuite la magnifique page du sith qui doit être `hello.$IP_DU_LOAD_BALANCER.sslip.io` soit directement en tapant l'url, soit depuis l'application d'administration de ArgoCD (vous avez le symbole "external link" sur l'application).

En bas à droite vous avez le lien de validation (sous la forme `challenge: GH{QmllbiBlc3NhecOpIG1haXMgY2Ugbidlc3QgcGFzIHVuIHZyYWlzIGNvZGUuLgo=}`), copiez collez le token de challenge (la partie `GH{...}`)
dans le formulaire de validation de WorldAventure.
