## Kustomize dans les runnners Gitlab

Vous allez intégrer dans CI/CD les instructions pour deployer du kustomize en spécifiant le dossier d'`overlay` qui correspond à l'environnement à déployer.

Pour utiliser un `overlay` particulier, par exemple celui de la moria que l'on déploierait dans le namespace dwarf (en étant positionné à la racine du projet) :

```shell
kustomize build ./overlays/moria/ | kubectl apply -n dwarf -f -
```

Ce qui donnerait dans le fichier .gitlab-ci.yaml

```yaml
# ...

helm-dev:
  stage: deploy-helm-exemple
  extends: .kubeconfig
  script:
    - echo "👋 Deploy helm dans le namespace dwarf"
    - kubectl create ns dwarf 2>/dev/null || true
    - kustomize build ./overlays/staging/ | kubectl apply -n dwarf -f -
# ...
```

**Depuis votre workspace gipods (ou gitlab avec le Web IDE intégré) `deploy-sith-from-gitlab`.**

À vous :

- d'ajouter dans le fichier `.gitlab-ci.yaml` le job permettrant de déployer la version de **production** dans le namespace **kustomize-prod**.
- et de modifier la configuration de production pour utiliser la version **1.0** du `sith`.
