## Intégration de Vault via un "Operator"

Dans cette aventure, vous allez avoir besoin de renforcer la sécurité votre `sith`. Pour cela, vous avez choisi d'utiliser [BanzaiCloud Bank Vault](https://github.com/banzaicloud/bank-vaults) qui vous permettra de distribuer de façon sécurisée vos différents secrets.

Pour cela vous allez :

- installer l'operator BanzaiCloud Bank Vault.
- utiliser cet operator pour "muter" la valeur du secret à monter dans votre pod.

Vous trouverez par ailleurs les différents type de deploiements (kustomize, helm, dhall).

Ces descripteurs vous permettront de :

- récupérer dans l'entrée du serveur vault `secret/james-secret` la valeur ayant pour clef `content` (via un Secret Kubernetes et des annotations)
- monter ce secret en tant que fichier "/etc/sith/secret" dans votre deployement

### Installer l'operator

**Depuis votre livre de sorts (répertoire `/workspace/spell-book/`).**  
Il faut que vous puissiez passer des commandes kubectl.

👀 Chapitre (aka sous dossier) `secrets/vault-operator`

Installer l'operator BanzaiCloud Bank Vault

```shell
./01-install-vault.sh
```

Ce script va :

- créer un namespace pour vault-operator.
- installer et preconfigurer vault dans le cluster (ne faites pas cela en production).
- installer l'operator vault
- créer une entrée vault `secret/james-secret` contenant la k/v `content="👋 Hello from Vault Operator :)"`

### Comment injecter un secret vault dans un secret kubernetes

Cette étape n'est pas à réaliser, elle est informative, vous devez seulement utiliser les différents manifest présent en les intégrant soit dans votre pipeline GitLab, soit dans ArgoCD.

Pour créer un secret dans vault, vous pouvez par exemple utiliser l'outil en ligne de commande vault qui est disponible dans le pod de Vault. Pour cet exercice ce ne sera pas nécessaire, le secret est déjà créé dans la phase initiale. La commande suivante est données à titre d'exemple.

```sh
kubectl -n vault-operator exec vault-0 -c vault -- /bin/sh -c "VAULT_TOKEN=$VAULT_TOKEN VAULT_CACERT=/vault/tls/ca.crt vault kv put secret/my-secret content='my very secure secret'"
```

L'injection de secret dans un manifest de pod par exemple ne nécessite pas de l'encoder en base64. Les descripteurs suivants sont donnés à titre d'exemple, regardez dans les configurations (dhall ou helm ou kustomize en fonction de ce que vous avez choisi précédemment). Les valeurs à remplacer sont identifiées via des "FIXME"

```sh
# Exemple d'un secret "my-secret" annoté permettant de récupérer le contenu du secret vault ayant comme path : "secret/data/my-secret" et comme clef "content".
SECRET=$(echo -n '${vault:secret/data/my-secret#content}' | base64 -w0)
cat > secret.yml <<EOF
---
apiVersion: v1
kind: Secret
metadata:
  name: my-secret
  annotations:
    vault.security.banzaicloud.io/vault-addr: "https://vault.vault-operator:8200"
    vault.security.banzaicloud.io/vault-tls-secret: "vault-tls"
    # Permet de notifier le webhook d'aller inspecter le contenu du secret.
    vault.security.banzaicloud.io/inline-mutation: 'true'
data:
  secret: ${SECRET}
EOF
kubectl apply -f secret.yml
```

Le secret peut alors être utilisé comme un secret classique.

```yaml
---
kind: Pod
apiVersion: apps/v1
metadata:
  name: secure-pod
spec:
...
containers:
  - ...
    volumeMounts:
    - mountPath: /etc/secrets
      name: secret-volume
  volumes:
    - name: secret-volume
      secret:
        secretName: my-secret
...
```

### Utiliser l'operator vault pour déployer notre sith

Vous utiliserez les différents manifest déjà présent en les intégrants soit dans votre pipeline GitLab, soit dans ArgoCD.
Comme précédement ces manifests sont dans le repo gitlab et suivent la même logique que les autres deployements (un fichier de valeurs ou un dossier d'overlays ayant un pattern `vault-operator` à utiliser dans votre processus de déploiement)

- La version du sith à déployer est la version `1.3`
- Le namespace qui doit être utilisé est `[dhall|helm|kustomize]-vault-operator-secret` (selon que vous choisissiez dhall ou helm ou kustomize)
- Renseignez les différents FIXME dans les configurations (dhall ou helm ou kustomize en fonction de ce que vous avez choisi précédemment)
