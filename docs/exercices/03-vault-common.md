# Vault

[HashiCorp Vault](https://www.hashicorp.com/products/vault) est une solution de gestion des secrets, sécurisée et robuste.  
Elle fournit une API permettant son intégration dans de nombreuses solutions dont kubernetes, permettant ainsi de déléguer le stockage des secrets en dehors de git.

Pour cette dernière quête, vous allez faire en sorte d'utiliser Vault pour gérer et persister vos secrets. Ces secrets seront injectés dans kubernetes.  
Vous trouverez des scripts permettant d'installer Vault (dans kubernetes pour des raisons de simplification) et son mécanisme d'injection dans kubernetes.
