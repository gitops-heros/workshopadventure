# Kubeseal

Dans cette aventure, vous allez avoir besoin de renforcer la sécurité votre `sith`. Pour cela, une des options pourra être l'utilisation du sceptre [Bitnami Kubeseal](https://github.com/bitnami-labs/sealed-secrets) qui vous permettra de chiffrer et de distribuer vos différents secrets.

## Chiffrer un secret

```sh
$ kubectl create secret generic my-token --from-literal=my_token='mysecret' --dry-run=client -o yaml | \
    kubeseal --cert public-cert.pem --format=yaml | \
    kubectl apply -f -
```

## Lister les secrets créés

```sh
$ kubectl get sealedsecret
NAME       AGE
my-token   4m14s
$ kubectl get secrets
NAME                  TYPE                                  DATA   AGE
default-token-ltrtq   kubernetes.io/service-account-token   3      37h
my-token              Opaque                                1      1s
```
