#!/usr/bin/python3

import binascii
import subprocess

res = subprocess.run(["/bin/sh", "-c", "kubectl get cm kube-root-ca.crt -o jsonpath='{.data.ca\.crt}' | openssl x509 -outform der"], capture_output=True)
print(binascii.crc32(res.stdout))