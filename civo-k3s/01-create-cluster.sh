#!/bin/bash
set -e
DIR=$(dirname "$0")
pushd $DIR

# civo region ls
CIVO_NETWORK_REGION=$(jq -r '.meta.default_region' $HOME/.civo.json)
# CIVO_NETWORK_REGION='NYC1' 
# CIVO_NETWORK_REGION='LON1'

# civo kubernetes size ls
#CLUSTER_SIZE='g4s.kube.small'
CLUSTER_SIZE='g4s.kube.medium'

# civo kubernetes version ls
# stable
K8S_VERSION='1.26.4-k3s1'

echo "🏗️ Creating the cluster"
set -x

if [ "x$1" == "x" ]
then
  name="gandalf"
else
  name=$1
fi
clustername="$name-cluster"

{ set +x; } 2> /dev/null # silently disable xtrace
[[ ! -f $HOME/.civo.json ]] && echo "Must initialize civo with: civo apikey save gitops <YOUR APIKEY>" && exit 1

CIVO_CURRENT_APIKEY=$(jq -r '.meta.current_apikey' $HOME/.civo.json)
if [ "$CIVO_CURRENT_APIKEY" == "" ]
then
    echo "Must initialize civo with: civo apikey save gitops <YOUR APIKEY>" && exit 1
fi

# Create if not exist kube-firewall
CIVO_APIKEY=$(jq -r ".apikeys | select(.$CIVO_CURRENT_APIKEY) | .[]" $HOME/.civo.json)
if [ "$CIVO_APIKEY" == "" ]
then
    echo "Must initialize civo with: civo apikey save gitops <YOUR APIKEY>" && exit 1
fi

# # TODO test via cmdline
CIVO_URL=$(jq -r '.meta.url' $HOME/.civo.json)
EXISTING_FIREWALL=$(curl -s --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls?region=$CIVO_NETWORK_REGION" | jq -r '.[] | select(.name == "kube-firewall")')
# with cmdline
 #EXISTING_FIREWALL=$(civo firewall ls -o json | jq -r '.[] | select(.name == "kube-firewall")')
if [ "$EXISTING_FIREWALL" == "" ]
then
    echo "🏗️ Creating civo firewall."

    CIVO_NETWORK_ID=$(curl -s --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/networks?region=$CIVO_NETWORK_REGION" | jq -r '.[] | select(.default) | .id')

    curl -s --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls" \
         -d "name=kube-firewall&network_id=$CIVO_NETWORK_ID&region=$CIVO_NETWORK_REGION"
    FIREWALL_ID=$(curl -s --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls?region=$CIVO_NETWORK_REGION" | jq -r '.[] | select(.name == "kube-firewall") | .id')
    FIREWALL_RULES=$(curl -s --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls/$FIREWALL_ID/rules?region=$CIVO_NETWORK_REGION" | jq -r '.[] | select(.direction == "ingress") | .id')
    echo "🏗️ Deleting default rules."
    for rule in ${FIREWALL_RULES}
    do
        curl -s -X DELETE --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls/$FIREWALL_ID/rules/$rule?region=$CIVO_NETWORK_REGION"
    done
    echo "🏗️ Create k8s rules."
    curl -s -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls/$FIREWALL_ID/rules?region=$CIVO_NETWORK_REGION" \
         -d "region=$CIVO_NETWORK_REGION&protocol=tcp&start_port=80,443,6443"
fi

set -x

# using existing firewall
echo "civo k3s create ${clustername} -r=Traefik-v2-loadbalancer -r=Traefik-v2-nodeport --existing-firewall kube-firewall --size $CLUSTER_SIZE --version $K8S_VERSION --nodes 2 --region $CIVO_NETWORK_REGION --wait --save --yes"
civo k3s create ${clustername} -r=Traefik-v2-loadbalancer -r=Traefik-v2-nodeport --existing-firewall kube-firewall --size $CLUSTER_SIZE --version $K8S_VERSION --nodes 2 --region $CIVO_NETWORK_REGION --wait --save --yes

# civo kubernetes config ${clustername} --region $CIVO_NETWORK_REGION --save --overwrite

chmod 600 ~/.kube/config

kubectl version
kubectl cluster-info

# <!> for using custom traefik => uninstall helm traefik before `helm uninstall traefik -n kube-system`
{ set +x; } 2> /dev/null # silently disable xtrace

## wait for coredns
echo "⏳ Waiting for CoreDNS to be ready"
sleep 10
kubectl wait deployment coredns -n kube-system --timeout=-1s --for condition=available
while [ "1" != $(kubectl get deployment coredns -n kube-system -o=custom-columns=READY:.status.readyReplicas --no-headers) ]
do
    printf "."
    sleep 1
done
echo " ✅"
echo "CoreDNS is Ready"


echo "🚦 Install Traefik 2.5"
set -x
kubectl apply -f ./traefik/ --wait

{ set +x; } 2> /dev/null # silently disable xtrace


echo "⏳ Waiting for Traefik to be ready"
sleep 5
kubectl wait deployment traefik-ingress-controller -n kube-system --timeout=-1s --for condition=available
while [ "1" != $(kubectl get deployment traefik-ingress-controller -n kube-system -o=custom-columns=READY:.status.readyReplicas --no-headers) ]
do
    printf "."
    sleep 1
done
echo " ✅"

set +e
sleep 10
IP=$(kubectl get svc traefik-ingress-controller -n kube-system -o jsonpath='{ .status.loadBalancer.ingress[0].ip }')

if [[ "x$IP" != "x" ]]
then
  echo "📮 IP du cluster $IP"
  echo "Call in deploy-sith repository change-ip.sh $IP"
else
  echo "Load balancer en cours de mise en place, vous pourrez récupérer l'ip du cluster avec le script ./get-cluster-ip.sh"
  echo "Pour si le load balancer est prêt vous pouvez aussi utiliser la commande kubectl get svc traefik-ingress-controller -n kube-system (si le champ EXTERNAL-IP est renseigné c'est ok)"
fi

popd
